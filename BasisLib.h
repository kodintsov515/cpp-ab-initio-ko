#pragma once

#include <string>
#include <map>
#include <vector>
#include <math.h>
#include <fstream>
#include <iostream>

class BasisType
{
public:
    int CoreGTOs{6};
    int split{2};
    int FirstSplitGTOs{3};
    int SecondSplitGTOs{1};
    int ThirdSplitGTOs{0};
    bool PolOnHeavy{false};
    bool PolOnLight{false};
    bool DiffOnHeavy{false};
    bool DiffOnLight{false};
    BasisType FillData(std::string name);
};

struct oneTerm_t {
    double a{0};
    double c{0};
};

using radial_t = std::vector<oneTerm_t>;

class BasisLib 
{
public:
    void loadFromGamessUS(const std::string& filename);
    static int getT(const std::string& name);
    BasisLib();
protected:
    std::map<int, std::vector<std::vector<radial_t>>> content{};  // номер оболочки это индекс в векторе
private:
    BasisType bt;
    void ReadGTOs(std::vector<std::vector<radial_t>> &AtomicBS, std::ifstream& inp, int row);
    void normalize(std::vector<std::vector<radial_t>>& BSonAtom, int row);
    void calc_N(radial_t& orbital, const char* orb_type, int row); 
    const int L_MAX{5};
    static inline std::map<std::string, int> dict = {
        {"UNDEF", -2}, {"L", -1}, {"S", 0}, {"P", 1},
        {"D", 2},      {"F", 3},  {"G", 4}, {"H", 5}};
    static inline std::map<std::string, BasisType> BasisDict;
};

#include "BasisLib.h"
#include "Molecule.h"
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>

BasisLib::BasisLib()
{
    BasisType basis;
    BasisDict.insert(std::pair<std::string,BasisType>("6-21G", basis.FillData("6-21G")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31++G", basis.FillData("6-31++G")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31++G*", basis.FillData("6-31++G*")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31++G**", basis.FillData("6-31++G**")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31+G", basis.FillData("6-31+G")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31+G*", basis.FillData("6-31+G*")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31+G**", basis.FillData("6-31+G**")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31G*", basis.FillData("6-31G*")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31G**", basis.FillData("6-31G**")));
    BasisDict.insert(std::pair<std::string,BasisType>("6-31G", basis.FillData("6-31G")));
}

BasisType BasisType::FillData(std::string name)
{
    BasisType basis;
    if(name == "6-21G")
    {
        basis.FirstSplitGTOs=2;
    }
    if(name == "6-31++G")
    {
        basis.DiffOnHeavy=true;
        basis.DiffOnLight=true;
    }
    if(name == "6-31++G*")
    {
        basis.DiffOnHeavy=true;
        basis.DiffOnLight=true;
        basis.PolOnHeavy=true;
    }
    if(name == "6-31++G**")
    {
        basis.PolOnHeavy=true;
        basis.PolOnLight=true;
        basis.DiffOnHeavy=true;
        basis.DiffOnLight=true;
    }
    if(name == "6-31+G")
    {
        basis.DiffOnHeavy=true;
    }
    if(name == "6-31+G*")
    {
        basis.PolOnHeavy=true;
        basis.DiffOnHeavy=true;
    }
    if(name == "6-31+G**")
    {
        basis.PolOnHeavy=true;
        basis.PolOnHeavy=true;
        basis.DiffOnLight=true;
    }
    if(name == "6-31G*")
    {
        basis.PolOnHeavy=true;
    }
    if(name == "6-31G**")
    {
        basis.PolOnHeavy=true;
        basis.PolOnLight=true;
    }
    return basis;
}

void BasisLib::loadFromGamessUS(const std::string& filename) 
{
    std::ifstream inp(filename);
    if (not inp) throw std::runtime_error("Problem with *.basis file");
    for(int i=0; i<10;i++) // читаем первые 10 строк, чтобы определить базис
    {
        std::string line;
        std::getline(inp, line);
        int l = line.find("Basis set:"); 
        if(l!=std::string::npos) // нашли строку, содержащую имя базиса
        {
            std::string basis;
            int p = l+11;
            basis.assign(line,p,10); // извлекаем имя базиса
            for(int j = 0; j < basis.length(); j++) //удаляем возможные пробелы из имени базиса
            {
                if(basis[j] == ' ') 
                {
                    basis.erase(j,1);
                    j--;
                }
            }
            std::map<std::string, BasisType>::iterator itr = BasisDict.find(basis); // ищем имя базиса в словаре
            bt = itr->second; // присваиваем параметры базиса
            // закончили с поиском имени базиса
        }
        while(getline(inp, line))
        {
            if(line == "" | line == "$DATA") {} // пропускаем все строчки вплоть до названия первого атома
            else
            {
                for(int i=0; i<10; i++) // идем по именам атомов
                {
                    getline(inp, line); // считывает строчку, содержащую имя атома
                    int atom_charge = -1;
                    atom_charge = Molecule::getQ(line); // вычисляем заряд ядра по имени атома
                    std::vector<std::vector<radial_t>> AtomicBS; // сюда будут писаться все радиальные базисные параметры
                    int row;
                    if(atom_charge<=2) row=1; // определяем период элемента
                    else row=2;
                    this->ReadGTOs(AtomicBS, inp, row); // последовательно считываем параметры для всех GTO для данного атома. поляризационные и диффузные функции учитываются
                    this->normalize(AtomicBS, row); // нормируем c_i. контаминанты включены в нормировку
                    content.insert(std::pair<int,std::vector<std::vector<radial_t>>>(atom_charge, AtomicBS)); // записываем отнормированные радиальные параметры в библиотеку
                }
            }
        }
    }
    inp.close();
}

void BasisLib::ReadGTOs(std::vector<std::vector<radial_t>> &AtomicBS, std::ifstream& inp, int row)
{
    if(row==1) // чтение базиса для легких элементов
    {
        char type;
        int numberofGTO;
        inp >> type >> numberofGTO; // type и numberofGTO нигде не использются, т.к. нам уже известны параметры базиса
        int number;
        oneTerm_t oneGTO;
        for(int i=0; i<bt.FirstSplitGTOs; i++)
        {
            inp >> number >> oneGTO.a >> oneGTO.c;
            AtomicBS[0][0].push_back(oneGTO);
        }
        inp >> type >> numberofGTO;
        for(int i=0; i<bt.SecondSplitGTOs; i++)
        {
            inp >> number >> oneGTO.a >> oneGTO.c;
            AtomicBS[0][0].push_back(oneGTO);
        }
        inp >> type >> numberofGTO;
        for(int i=0; i<bt.ThirdSplitGTOs; i++) // для случая трижды расщепленных попловских базисов
        {
            inp >> number >> oneGTO.a >> oneGTO.c;
            AtomicBS[0][0].push_back(oneGTO);
        }
        if(bt.DiffOnLight==true)
        {
            inp >> type >> numberofGTO;
            inp >> number >> oneGTO.a >> oneGTO.c;
            AtomicBS[0][0].push_back(oneGTO);
        }
        if(bt.PolOnLight==true)
        {
            inp >> type >> numberofGTO;
            inp >> number >> oneGTO.a >> oneGTO.c;
            AtomicBS[1][0].push_back(oneGTO); // в нашей библиотеке поляризационным функциям s орбиталей будут соответствовать 1p орбитали
        }
    }
    if(row==2) // чтение базиса для тяжелых элементов
    {
        char type;
        int numberofGTO;
        inp >> type >> numberofGTO;
        int number;
        oneTerm_t sGTO,pGTO;
        for(int i=0; i<bt.CoreGTOs; i++)
        {
            inp >> number >> sGTO.a >> sGTO.c;
            AtomicBS[0][0].push_back(sGTO);
        }
        inp >> type >> numberofGTO;
        for(int i=0; i<bt.FirstSplitGTOs; i++)
        {
            inp >> number >> sGTO.a >> sGTO.c >> pGTO.c;
            pGTO.a = sGTO.a; // не забываем про общий экспоненциальный множитель
            AtomicBS[0][1].push_back(sGTO);
            AtomicBS[1][1].push_back(pGTO);
        }
        inp >> type >> numberofGTO;
        for(int i=0; i<bt.SecondSplitGTOs; i++)
        {
            inp >> number >> sGTO.a >> sGTO.c >> pGTO.c;
            pGTO.a = sGTO.a;
            AtomicBS[0][1].push_back(sGTO);
            AtomicBS[1][1].push_back(pGTO);
        }
        inp >> type >> numberofGTO;
        for(int i=0; i<bt.ThirdSplitGTOs; i++)
        {
            inp >> number >> sGTO.a >> sGTO.c >> pGTO.c;
            pGTO.a = sGTO.a;
            AtomicBS[0][1].push_back(sGTO);
            AtomicBS[1][1].push_back(pGTO);
        }
        if(bt.DiffOnHeavy==true)
        {
            inp >> type >> numberofGTO;
            inp >> number >> sGTO.a >> sGTO.c >> pGTO.c;
            pGTO.a = sGTO.a; 
            AtomicBS[0][1].push_back(sGTO);
            AtomicBS[1][1].push_back(pGTO);
        }
        if(bt.PolOnHeavy==true)
        {
            inp >> type >> numberofGTO;
            inp >> pGTO.a >> pGTO.c; // тут логичнее завести переменную dGTO
            AtomicBS[2][0].push_back(pGTO);
        }
    }
}

void BasisLib::normalize(std::vector<std::vector<radial_t>>& BSonAtom, int row)
{
    if(row==1)
    {
        for(int i=0; i < BSonAtom.size(); i++)
            for(int j=0; j < BSonAtom[i].size(); j++)
            {
                if(i==0)
                    this->calc_N(BSonAtom[i][j], "S", row);
                if(i==1)
                    this->calc_N(BSonAtom[i][j], "P", row);
                if(i==2)
                    this->calc_N(BSonAtom[i][j], "D", row);
            }
    }
}

void BasisLib::calc_N(radial_t& orbital, const char* orb_type, int row)
{
    if(orb_type == "S")
    {
        double N=1, sum=0;
        if(row==2) // для элементов 2 периода проводится нормировка остовновных базисных функций
        { 
            for(int i=0; i<bt.CoreGTOs;i++) // нормировка остовных базисных функций
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=0; i<bt.CoreGTOs;i++) // перезаписываем нормированные коэффициенты
                orbital[i].c=N*orbital[i].c;
            for(int i=bt.CoreGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs;i++) // нормировка первого split'а для элементов 2 периода
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=bt.CoreGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
            for(int i=bt.CoreGTOs+bt.FirstSplitGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs;i++) // нормировка второго split'а для элементов 2 периода
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=bt.CoreGTOs+bt.FirstSplitGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
            for(int i=bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;i++) // нормировка тертьего split'а
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<bt.CoreGTOs+bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
        }
        if(row == 1)
        {
            for(int i=0; i<bt.FirstSplitGTOs; i++) // нормировка первого split'а для элементов 1 периода
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=0; i<bt.FirstSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
            for(int i=bt.FirstSplitGTOs; i<bt.FirstSplitGTOs+bt.SecondSplitGTOs;i++) // нормировка второго split'а для элементов 1 периода
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=bt.FirstSplitGTOs; i<bt.FirstSplitGTOs+bt.SecondSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
            for(int i=bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;i++) // нормировка тертьего split'а
                sum += (orbital[i].c*orbital[i].c)/pow(2*orbital[i].a, 1.5);
            N=sqrt(8/(sum*pow(tgamma(0.5), 3)));
            for(int i=bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;i++)
                orbital[i].c=N*orbital[i].c;
        }
        if(bt.DiffOnLight==true) // нормировка диффузных функций, если они есть
        {
            int ndiff = bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;
            orbital[ndiff].c=orbital[ndiff].c*sqrt(8*pow(2*orbital[ndiff].a, 1.5))/(pow(orbital[ndiff].c, 2)*pow(tgamma(0.5), 3));
        }
    }
    if(orb_type == "P")
    {
        double N=1, sum=0;
        if(bt.PolOnLight==true)
            orbital[0].c=orbital[0].c*sqrt(8*pow(2*orbital[0].a, 2.5)/(pow(orbital[0].c, 2)*tgamma(1.5)*pow(tgamma(0.5), 2))); // нормировка поляризационной функции для 1s;
        if(row==2)
        {
            for(int i=1; i<bt.FirstSplitGTOs+1; i++) // нормировка первого split'а. Коровых p функций нет 
                sum+=(orbital[i].c*orbital[i].c)/(pow(2*orbital[i].a,2.5));
            N=sqrt(8/(sum*tgamma(1.5)*pow(tgamma(0.5), 2)));
            for(int i=1; i<bt.FirstSplitGTOs+1; i++)
                orbital[i].c=orbital[i].c*N;
            for(int i=1+bt.FirstSplitGTOs; i<1+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i++) // нормировка второго split'а.
                sum+=(orbital[i].c*orbital[i].c)/(pow(2*orbital[i].a,2.5));
            N=sqrt(8/(sum*tgamma(1.5)*pow(tgamma(0.5), 2)));
            for(int i=1+bt.FirstSplitGTOs; i<1+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i++)
                orbital[i].c=orbital[i].c*N;
            for(int i=1+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<1+bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs; i++) // нормировка третьего split'а.
                sum+=(orbital[i].c*orbital[i].c)/(pow(2*orbital[i].a,2.5));
            N=sqrt(8/(sum*tgamma(1.5)*pow(tgamma(0.5), 2)));
            for(int i=1+bt.FirstSplitGTOs+bt.SecondSplitGTOs; i<1+bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs; i++)
                orbital[i].c=orbital[i].c*N;
            if(bt.DiffOnHeavy==true) // нормировка диффузных p функций, если они есть
            {
                int ndiff = 1+bt.FirstSplitGTOs+bt.SecondSplitGTOs+bt.ThirdSplitGTOs;
                orbital[ndiff].c=orbital[ndiff].c*sqrt(8*pow(2*orbital[ndiff].a, 2.5)/(pow(orbital[ndiff].c, 2)*tgamma(1.5)*pow(tgamma(0.5), 2)));
            }
        }
    }
    // поляризационные d орбитали не нормированы, т.к. с этим возники теоретические трудности
}